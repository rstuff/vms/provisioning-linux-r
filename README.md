# R Provisioning (Linux)

Scripts to provision a Vagrant box (with a linux like) with R and R-devel environment.

## Purpose

This scripts provision a Vagrant Box with R and R-devel environement.  
It use Packer (for box management) and Ansible to provision the VM.

> Boxes generated are availables here : [https://app.vagrantup.com/VMR/boxes/LinuxMint20-R](https://app.vagrantup.com/VMR/boxes/LinuxMint20-R)  

## Requirements

* [Packer](https://www.packer.io/downloads)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads) (>= 6.1.14)
* [Packer](https://www.packer.io/)
* [Ansible](https://www.ansible.com/) (>= 2.9)
* [Vagrant](https://www.vagrantup.com/) (optional)

## Files

* [packer\_template.json](packer_template.json): packer template
* [playbook.yml](playbook.yml): ansible playbook
* [roles/](roles/): ansible roles
* [group\_vars](group_vars): ansibe inventory (for debug)
* [VagrantFile](VagrantFile): vagrant file (for debug)

## Build

> edit [build.sh](build.sh)

Build and upload box
```
packer build \
  -var "vmname=${VMNAME}" \
  -var "vagrant_box=${VAGRANT_BOX}" \
  -var "headless=${HEADLESS}" \
  -var "version=${R_VERSION}" \
  -var "vagrantcloud_token=${VAGRANTCLOUD_TOKEN}" \
  -var "boxtag=${BOX_TAG}" \
  -var "version_description=${DESCRIPTION}" \
  packer_template.json
```

Local instantiation
```
vagrant up --provision
```

## LICENSE

[MIT](LICENSE)

## Authors

* [Jean-François Rey](https://gitlab.com/_jfrey_)

