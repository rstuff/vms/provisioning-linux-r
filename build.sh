#!/bin/bash -eux


OS="LinuxMint20"
VAGRANT_BOX="../packer-linux/box/virtualbox/linuxmint-cinnamon-20.0-4.0.3.1608288458.box"
HEADLESS=false
BOX_VERSION="4.3.1"
R_VERSION="4.3.1"
VAGRANTCLOUD_TOKEN=""
BOX_TAG="VMR/${OS}-R"
DESCRIPTION="${OS} + R ${R_VERSION} + R-devel"

VM_NAME="${OS}-R${R_VERSION}"

### Get a box and provision it with ansible playbook.yml

packer build \
  -force \
  -var "vm_name=${VM_NAME}" \
  -var "vagrant_box=${VAGRANT_BOX}" \
  -var "headless=${HEADLESS}" \
  -var "version=${R_VERSION}" \
  -var "vagrantcloud_token=${VAGRANTCLOUD_TOKEN}" \
  -var "boxtag=${BOX_TAG}" \
  -var "version_description=${DESCRIPTION}" \
  packer_template.json 

